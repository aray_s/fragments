package com.`as`.gettingfamiliarwithfragments

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.commit
import com.`as`.gettingfamiliarwithfragments.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var secondFragmentIsFirst = true
    private var secondFragmentColor = getRandomColor()
    private var thirdFragmentColor = getRandomColor()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState == null) {
            setColors()
            supportFragmentManager.commit { add(binding.fragment1.id, Fragment1()) }
            setFragments(false)
        } else {
            secondFragmentIsFirst = savedInstanceState.getBoolean("SECOND_FRAGMENT_FIRST")
            secondFragmentColor = savedInstanceState.getInt("SECOND_FRAGMENT_COLOR")
            thirdFragmentColor = savedInstanceState.getInt("THIRD_FRAGMENT_COLOR")
            binding.fragment2.setBackgroundColor(secondFragmentColor)
            binding.fragment3.setBackgroundColor(thirdFragmentColor)
            if (!secondFragmentIsFirst) setFragments(true)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("SECOND_FRAGMENT_FIRST", secondFragmentIsFirst)
        outState.putInt("SECOND_FRAGMENT_COLOR", secondFragmentColor)
        outState.putInt("THIRD_FRAGMENT_COLOR", thirdFragmentColor)
    }

    fun swapFragments() {
        if (secondFragmentIsFirst) setFragments(true)
        else setFragments(false)
        secondFragmentIsFirst = !secondFragmentIsFirst
    }

    fun changeColors() {
        secondFragmentColor = getRandomColor()
        thirdFragmentColor = getRandomColor()
        setColors()
    }

    private fun getRandomColor() =
        Color.rgb(Random().nextInt(256), Random().nextInt(256), Random().nextInt(256))

    private fun setColors() {
        binding.fragment2.setBackgroundColor(secondFragmentColor)
        binding.fragment3.setBackgroundColor(thirdFragmentColor)
    }

    private fun setFragments(i: Boolean) = if (i)
        supportFragmentManager.commit {
            replace(binding.fragment2.id, Fragment3())
            replace(binding.fragment3.id, Fragment2())
        }
    else
        supportFragmentManager.commit {
            replace(binding.fragment2.id, Fragment2())
            replace(binding.fragment3.id, Fragment3())
        }
}